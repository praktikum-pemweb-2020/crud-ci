<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nama Pelanggan</title>
</head>

<body>
    <a href=<?= base_url("customer/add_new"); ?>>
        Add New Customer
    </a>
    <table border="1">
        <thead>
            <tr>
                <th>Nama Pertama</th>
                <th>Nama Kedua</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($customer as $row) : ?>
                <tr>
                    <td><?= $row['customer_firstname']; ?></td>
                    <td><?= $row['customer_lastname']; ?></td>
                    <td>
                        <?= anchor("customer/edit/{$row['customer_id']}", "Edit") ?>
                    </td>
                    <td>
                        <a href=<?= base_url("customer/delete/{$row['customer_id']}"); ?>>
                            Delete
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>

</html>