<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add New Customer</title>
</head>

<body>
    <form action=<?= base_url("/customer/save") ?> method="post">
        <p>First Name : <input type="text" name="customer_firstname"></p>
        <p>Last Name : <input type="text" name="customer_lastname"></p>
        <button type="submit">Save</button>
    </form>
</body>

</html>